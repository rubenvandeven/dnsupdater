# DNS Updater

A dyndns-like updater for the DNS service of PCExtreme, called Aurora. Their service is supported by Apache's libcloud ([docs](https://libcloud.readthedocs.io/en/latest/dns/drivers/auroradns.html)). This simple script helps to use these domains for some [homebrewing](https://homebrewserver.club/).

## Usage

It works fairly simple. Create a `config.yaml` file:

```yaml
client_key: YOURKEY
client_secret: YOURSECRET

zones:
  - domainname.com:
      - name: www
        type: AAAA
      - name: ""
        type: AAAA
      - name: www
        type: A
      - name: ""
        type: A


```

and run:

```bash
python dnsupdate.py -c config.yaml
```

You should probably run it with cron to keep your DNS up to date. If you have any issues, add `-v` for some verbosity.

The script is _very_ basic: so `AAAA` records will be set to your IPv6 address, all other record types to IPv4.
